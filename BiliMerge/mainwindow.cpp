#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <QList>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);
    p = new QProcess(this);
    // 内置ffmpeg, 经测试不好用
//    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
//    env.insert("PATH",QDir::currentPath ()+";"+env.value("PATH"));
//    p->setProcessEnvironment(env);


    // 添加使用说明
    ui->result->insertPlainText("选择缓存的视频目录，点击 “执行合并” 即可！");
}

MainWindow::~MainWindow() { delete ui; }

// 拦截关闭窗口事件，结束正在运行的ffmpeg进程（如果有）
void MainWindow::closeEvent(QCloseEvent *event) {
    // 结束正常运行的程序
    p->kill();
    // 退出程序
    event->accept();
}

// 选择目录
void MainWindow::on_choesePath_clicked() {
    QString path = QFileDialog::getExistingDirectory(
        this, tr("请选择视频目录"), "d:/",
        QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    ui->biliPath->setText(path);

    ui->ResultPathText->setText(path+"/out");

}

// 执行合并
void MainWindow::on_doMerge_clicked() {
    // 清空结果面板
    ui->result->clear();

    // 封装为子线程，用于异步执行
    auto runFunc = [&](const QString &cmd, const QStringList &args) {
        // 禁用按钮，防止重复点击
        ui->doMerge->setDisabled(true);
        // 开始执行
        p->start(cmd, args);
        while (!p->waitForFinished(5 * 1000)) {
            // 更新UI，通知处理过程
            ui->result->insertPlainText("...");
        }
        ui->result->insertPlainText("合并完成！\n\n");
        //    p->close();
        ui->doMerge->setDisabled(false);
        // 滚动到最新文本位置
        ui->result->moveCursor(QTextCursor::EndOfLine);
    };

    // 禁用按钮，防止重复点击
    ui->doMerge->setDisabled(true);

    // 使用程序目录的ffmpeg
    auto cmd = QString(QDir::currentPath () + "/ffmpeg.exe");

    QString path = ui->biliPath->text();
    QString RetPath = ui->ResultPathText->text();
    QDir dir;
    if (!dir.exists(RetPath))
    {
        dir.mkpath(RetPath);

    }


    QList<QString> list;
    FindEntryJson(list, path);

    foreach (QString file, list) {

        QFileInfo FileInfo(file);

        QString tmpName = FileInfo.dir().entryList(FileInfo.dir().AllDirs).at(2);
        QString video = FileInfo.dir().path() + "/" + tmpName + "/video.m4s";
        QString audio = FileInfo.dir().path()  + "/" + tmpName + "/audio.m4s";

        // 判断音视频是文件是否存在，不存在不处理
        if(!QFileInfo(audio).exists()||!QFileInfo(video).exists()){
            ui->result->insertPlainText(FileInfo.dir().path() +
                                        "\n\t 未获取到音频或视频，跳过！\n\n");
            continue;
        }


        QFile entry(file);
        entry.open(QIODevice::ReadOnly);
        QByteArray data = entry.readAll();
        entry.close();
        QJsonObject json = QJsonDocument::fromJson(data).object();
        // 获取当时视频标题
        QString title = json.value("page_data").toObject().value("part").toString();
        // 如果是动漫，不能正确获取，加一个判断
        if (title.isEmpty()) {
            title = json.value("ep").toObject().value("index_title").toString();
        }
        // 单个视频可能没有设置子元素，如果没有获取到就使用title
        if (title.isEmpty()) {
            title = json.value("title").toString();
        }
        // 如果还没有获取到标题使用 bvid
        if (title.isEmpty()) {
            title = json.value("bvid").toString();
        }
        // 如果还没有获取到标题使用 avid
        if (title.isEmpty()) {
            title = json.value("avid").toString();
        }

        // 如果还没有获取到标题不处理
        if (title.isEmpty()) {
            ui->result->insertPlainText(file +
                                        "\n\t视频标题获取失败，跳过！\n\n");
            continue;
        }


        QString final = (RetPath + "/" + title + ".mp4").replace(" ","_");

        // 判断生成的文件是否存在，如果存在，就跳过
        QFile finalFile(final);
        if (finalFile.exists()) {
            ui->result->insertPlainText(final +
                                        "\n\t合并后的文件已存在，跳过！\n\n");
            continue;
        }

        // 封装参数
        auto args = QStringList();
        // 快速处理
        args << "-i" << video << "-i" << audio << "-codec"
             << "copy" << final;
        // 转码处理，速度较慢
        //  ql << "-i"
        //     << "video.m4s"
        //     << "-i"
        //     << "audio.m4s"
        //     << "-c:v"
        //     << "libx264"
        //     << "-strict"
        //     << "-2"
        //     << "output.mp4";

        ui->result->insertPlainText(FileInfo.dir().Name + "\t" + title + "\n\t正在合并中...");

        QEventLoop loop;
        QFutureWatcher<void> wacher;

        connect(&wacher, &QFutureWatcher<void>::finished, &loop, &QEventLoop::quit);

        // 此行异步执行，不会阻塞！
        wacher.setFuture(QtConcurrent::run(runFunc, cmd, args));
        // 阻塞等待完成，在进行下一个！
        loop.exec();
    }
    ui->result->insertPlainText("\n\t所选的目录视频已合并完成！");
    QMessageBox::information(this, "提示信息", "所选的目录视频已合并完成!",
                             QMessageBox::Ok);
    ui->doMerge->setDisabled(false);
}

QStringList MainWindow::GetSubdirectories(QString path) {
    QDir dir(path);

    return dir.entryList(dir.AllDirs);
}

void MainWindow::on_ChoeseResultPathBtn_clicked()
{
    QString path = QFileDialog::getExistingDirectory(
        this, tr("请选择视频目录"), "d:/",
        QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

    ui->ResultPathText->setText(path);
}

void MainWindow::FindEntryJson(QList<QString> &entryJsonList, const QString &Path)
{
    QDir dir(Path);   //QDir的路径一定要是全路径，相对路径会有错误

    if(!dir.exists())
        return ;

    //取到所有的文件和文件名，去掉.和..文件夹
    dir.setFilter(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot);
    dir.setSorting(QDir::DirsFirst);
    //将其转化为一个list
    QFileInfoList list = dir.entryInfoList();
    if(list.size()<1)
        return ;

    int i = 0;
    //采用递归算法
    do {
        QFileInfo fileInfo = list.at(i);
        bool bisDir = fileInfo.isDir();
        if(bisDir)
            FindEntryJson(entryJsonList,fileInfo.filePath());
        else{
            if(fileInfo.fileName().endsWith("entry.json")){
                entryJsonList.append(fileInfo.filePath());
            }
        }
        ++i;
    }while(i<list.size());

    return ;
}

