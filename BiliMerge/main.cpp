#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);
    // 设置任务栏图标
  a.setWindowIcon(QIcon(":/icon/app.ico"));
  MainWindow w;
  w.show();

  return a.exec();
}
