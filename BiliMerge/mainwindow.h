#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "QFileDialog"
#include "QProcess"
#include <QCloseEvent>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMainWindow>
#include <QMessageBox>
#include <QTextCursor>
#include <QtConcurrent>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();
  //  拦截关闭窗口事件
  void closeEvent(QCloseEvent *event);

  QStringList GetSubdirectories(QString path);

private slots:

  void on_choesePath_clicked();

  void on_doMerge_clicked();

  void on_ChoeseResultPathBtn_clicked();

  void FindEntryJson(QList<QString>& entryJsonList, const QString& Path);

  private:
  Ui::MainWindow *ui;
  QProcess *p;
};
#endif // MAINWINDOW_H
