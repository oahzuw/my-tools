使用QT调用FFmpeg合并视频

```c++
/**
 *
 * 默认 vlc 可播放，转换速度快
 * ffmpeg -i video.m4s -i audio.m4s -codec copy v.mp4
 * 转换为：x264 格式 MP4 html 可播放，转换速度慢
 * ffmpeg -i video.m4s -i audio.m4s  -c:v libx264 -strict -2 output.mp4
 *
 * /
```



工具介绍欢迎关注公众号查看：**零一武**，扫码查看[文字介绍](https://mp.weixin.qq.com/s/gZ8ijdRh48jqaXFjWVV6bg)：


![wx_code](../images/url_code.png)


编译版下载
```bash
链接: https://pan.baidu.com/s/1-hxaMgqUd3tWG53dtmcBBg 
提取码: v51s
```